" number lines
set number
syntax on

" Tab stuff
set ai
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set background=dark
set backupdir=~/.vim/backup
set directory=~/.vim/backup
set laststatus=2

let g:ycm_key_list_select_completion = ['<TAB>', '<Down>', '<Enter>']
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" Pathogen plugin manager
execute pathogen#infect()

